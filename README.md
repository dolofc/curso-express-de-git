# README #
## INDICE ##

+ [Para que es este repositorio?](#markdown-header-para-que-es-este-repositorio)

+ [Qué-aprenderé en el Curso express de Git](#markdown-header-que-aprendere-en-el-curso-express-de-git)

+ [Objetivos del curso](#markdown-header-objetivos-del-curso)

+ [1.-Conocimientos iniciales sobre Git](#markdown-header-1-conocimientos-iniciales-sobre-git)

+ [2.- Instalación y configuración inicial de Git](#markdown-header-2-instalacion-y-configuracion-inicial-de-git)

+ [3.- Operativas básicas con Git](#markdown-header-3-operativas-basicas-con-git)

+ [4.- Clonado de repositorios de GitHub](#markdown-header-4-clonado-de-repositorios-de-github)

+ [5.- Subir y actualizar un repositorio Git en local a GitHub y BitBucket](#markdown-header-5-subir-y-actualizar-un-repositorio-git-en-local-a-github-y-bitbucket)

+ [6.- .gitignore y borrado de archivos del repositorio](#markdown-header-6-gitignore-y-borrado-de-archivos-del-repositorio)

+ [7.- Ramas y GitHub](#markdown-header-8-ramas-y-gitHub)

+ [8.- Fusionar ramas con Git merge](#markdown-header-7-fusionar-ramas-con-git-merge)

+ [9.- Pull request](#markdown-header-9-pull-request)

+ [10.- Publicar prácticas de cursos a través del sistema de GitHub Classroom](#markdown-header-10-publicar-practicas-de-cursos-a-través-del-sistema-de-github-classroom)

### Para que es este repositorio? ###

* Es un breve ***manual*** sobre git como repositorio de control de versiones.
* Version 01.00 
* [Manual de Git](https://desarrolloweb.com/manuales/manual-de-git.html) 	
* [Git](https://desarrolloweb.com/home/git)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
+  [Volver al INDICE](#markdown-header-indice)

### Qué aprenderé en el Curso express de Git ###

* Saber qué es Git y GitHub
* Iniciar Git para mantener las versiones del código de un proyecto en local
* Hacer commit de código en el proyecto
* Clonar repositorios de Github
* Sincronizar nuestro repositorio local con el repositorio remoto (GitHub)
* Enviar prácticas de los cursos por medio de GitHub Classroom
+  [Volver al INDICE](#markdown-header-indice)

### Objetivos del curso ###

En este curso la idea es ver una rápida introducción a Git, explicando de manera práctica las operativas más básicas con el sistema de control de versiones, como inicializar los repositorios, cómo enviar cambios al proyecto, cómo trabajar con Github, etc.
El objetivo es ofrecer una formación acelerada de Git, para que sepamos lo necesario para poder usar el sistema de control de versiones y poder restaurar los repositorios de GitHub de los ejemplos de los cursos, así como presentar las prácticas solicitadas por medio de GitHub Classroom.

+  [Volver al INDICE](#markdown-header-indice)

### 1.- Conocimientos iniciales sobre Git ###
* 1
![Sistema de control de versiones](https://bitbucket.org/dolofc/curso-express-de-git/raw/05a9fd0a08e11d572ce53acd39302a06158d0f60/repos_imgs/git01.png) 
* 2
![Sistema de control de versiones](https://bitbucket.org/dolofc/curso-express-de-git/raw/05a9fd0a08e11d572ce53acd39302a06158d0f60/repos_imgs/git02.png) 
* 3
![Sistema de control de versiones](https://bitbucket.org/dolofc/curso-express-de-git/raw/05a9fd0a08e11d572ce53acd39302a06158d0f60/repos_imgs/git03.png) 
* 4
![Sistema de control de versiones](https://bitbucket.org/dolofc/curso-express-de-git/raw/05a9fd0a08e11d572ce53acd39302a06158d0f60/repos_imgs/git04.png) 
* 5
![Sistema de control de versiones](https://bitbucket.org/dolofc/curso-express-de-git/raw/05a9fd0a08e11d572ce53acd39302a06158d0f60/repos_imgs/git05.png) 
* 6
![Sistema de control de versiones](https://bitbucket.org/dolofc/curso-express-de-git/raw/05a9fd0a08e11d572ce53acd39302a06158d0f60/repos_imgs/git06.png) 

+  [Volver al INDICE](#markdown-header-indice)

### 2.- Instalación y configuración inicial de Git ###
* En este vídeo vamos a ver cómo instalar Git y cómo realizar las configuraciones iniciales de Git para decirle quienes somos y poder firmar las actualizaciones de código. Además veremos cómo trabajar con un par de programas de terminal.
    * [ git--local-branching-on-the-cheap ](https://git-scm.com/)
* Veremos cómo instalar Git sobre el sistema Windows y cómo realizar los comandos iniciales de configuración de nuestro nombre y email, con el que se van a firmar los commits.
    * `git ini` se inicia el git
    * `git add .` se añade al stage
    * `git commit -m 'primer commit'` se sube al repositorio
* Veremos cómo realizar operaciones básicas con el terminal de línea de comandos. Tratando un par de recomendaciones de terminal para Windows:
    * por un lado el git bash, que lo instala el propio git
    * por otro lado está [ Comander https://cmder.net/ ](https://cmder.net/) Cmder is a software package created out of pure frustration over the absence of nice console emulators on Windows. It is based on amazing software, and spiced up with the Monokai color scheme and a custom prompt layout, looking sexy from the start.

+  [Volver al INDICE](#markdown-header-indice) 

### 3.- Operativas básicas con Git ###
+ En este vídeo veremos cómo inicializar repositorios, cómo añadir archivos al staging area de Git y cómo realizar commit del código.
    + Crear un nuevo proyecto para usar git en ese proyecto con `git init` en el **directorio local**
----
> > **se crea un repositorio en local y se inicia vacío**
>
> E:\(...)\Manuales>**md git-express**
>
> E:\(...)\Manuales>**cd git-express** 
>
> /e/(...)/Manuales/git-express
>
> ***$ git init***
>
> > Initialized empty Git repository in E:/(...)/Manuales/git-express/.git/
>
> > **Convierte una carpeta normal de nuestro disco en un repositorio de git**
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ ls -la***
>
> > total 4
>
> > drwxr-xr-x 1 dolores 197609 0 nov.  5 10:46 ./
>
> > drwxr-xr-x 1 dolores 197609 0 nov.  5 08:31 ../
>
> > drwxr-xr-x 1 dolores 197609 0 nov.  5 10:46 .git/
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ touch index.html***
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ ls -la***
>
> > total 4
>
> > drwxr-xr-x 1 dolores 197609 0 nov.  5 10:55 ./
>
> > drwxr-xr-x 1 dolores 197609 0 nov.  5 08:31 ../
>
> > drwxr-xr-x 1 dolores 197609 0 nov.  5 10:46 .git/
>
> > -rw-r--r-- 1 dolores 197609 0 nov.  5 10:55 index.html
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git add index.html***
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git commit -m 'Primer commit'***
>
> > [master (root-commit) 4a525cf] Primer commit
>
> >  1 file changed, 0 insertions(+), 0 deletions(-)
>
> >  create mode 100644 index.html
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git status***
>
> > On branch master
>
> > nothing to commit, working tree clean
----
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ mkdir css***
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ touch css/estilos.css***
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git status***
>
> > On branch master
>
> > untracked files:
>
> >   (use "git add <file>..." to include in what will be committed)
>
> >         css/
>
> > nothing added to commit but > > untracked files present (use "git add" to track)
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ mkdir images***
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git status***
>
> > On branch master
>
> > Changes not staged for commit:
>
> >   (use "git add <file>..." to update what will be committed)
>
> > (use "git restore <file>..." to discard changes in working directory)
>
> >         modified:   index.html
>
> > untracked files:
>
> >   (use "git add <file>..." to include in what will be committed)
>
> >         css/
>
> >         images/
>
> > no changes added to commit (use "git add" and/or "git commit -a")
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git add .***
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git status***
>
> > On branch master
>
> > Changes to be committed:
>
> >   (use "git restore --staged <file>..." to unstage)
>
> >         new file:   css/estilos.css
>
> >         new file:   images/image.jpg
>
> >         modified:   index.html
>
> > Changes not staged for commit:
>
> >   (use "git add <file>..." to update what will be committed)
>
> >   (use "git restore <file>..." to discard changes in working directory)
>
> >         modified:   index.html
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git add .***
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git status***
>
> > On branch master
>
> > Changes to be committed:
>
> >   (use "git restore --staged <file>..." to unstage)
>
> >         new file:   css/estilos.css
>
> >         new file:   images/image.jpg
>
> >         modified:   index.html
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git commit -m 'Segundo commit'***
>
> > [master 1ba876c] Segundo commit
>
> >  3 files changed, 2 insertions(+)
>
> >  create mode 100644 css/estilos.css
>
> >  create mode 100644 images/image.jpg
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ ls -la***
>
> > total 5
>
> > drwxr-xr-x 1 dolores 197609  0 nov.  5 11:05 ./
>
> > drwxr-xr-x 1 dolores 197609  0 nov.  5 08:31 ../
>
> > drwxr-xr-x 1 dolores 197609  0 nov.  5 11:13 .git/
>
> > drwxr-xr-x 1 dolores 197609  0 nov.  5 11:03 css/
>
> > drwxr-xr-x 1 dolores 197609  0 nov.  5 11:16 images/
>
> > -rw-r--r-- 1 dolores 197609 14 nov.  5 11:12 index.html
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> $ git status
>
> > On branch master
>
> > Changes not staged for commit:
>
> >   (use "git add/rm <file>..." to update what will be committed)
>
> >   (use "git restore <file>..." to discard changes in working directory)
>
> >         deleted:    images/image.jpg
>
> > untracked files:
>
> >   (use "git add <file>..." to include in what will be committed)
>
> >         images/image2.png
>
> > no changes added to commit (use "git add" and/or "git commit -a")
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git commit -m 'Tercer commit'***
>
> > On branch master
>
> > Changes not staged for commit:
>
> >   (use "git add/rm <file>..." to update what will be committed)
>
> >   (use "git restore <file>..." to discard changes in working directory)
>
> >         deleted:    images/image.jpg
>
> > *untracked files:*
>
> >   (use "git add <file>..." to include in what will be committed)
>
> >         images/image2.png
>
> > no changes added to commit (use "git add" and/or "git commit -a")
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git add .***
>
> user@PCname MINGW64 ***/e/(...)/Manuales/git-express (master)***
>
> ***$ git commit -m 'Tercer commit'***
>
> > [master a802e9b] Tercer commit
>
> >  2 files changed, 0 insertions(+), 0 deletions(-)
>
> >  ***delete*** mode 100644 images/image.jpg
>
> >  ***create*** mode 100644 images/image2.png
----

+ Enviar archivos del **Working Directory** al **Staging Area**
    + ***git add .***
+ Envío del **Staging Area** al **Repository** con la operación de **commit**
    + ***git commit -m 'mensaje para el commit'*** 
+ Aquí vamos a aclarar de manera práctica el proceso para realizar las operaciones más básicas con Git.
+  [Volver al INDICE](#markdown-header-indice)

### 4.- Clonado de repositorios de GitHub ###
+ Ahora vamos a ver cómo clonar un repositorio que está alojado en remoto, en GitHub, y cómo restaurar sus dependencias para poder ponerlo en marcha.
+ En este vídeo vamos a aprender a clonar repositorios existentes en GitHub, de modo que los tengamos en local y podamos comenzar a trabajar con ellos, revisar su código o cambiar cualquier cosa.
----
> user@PCname MINGW64 ***/e/(---)/Manuales/sesion4***
>
>  ***$ git clone https://github.com/EscuelaIt/slide-components-litelement.git***
>
>  > Cloning into 'slide-components-litelement'...
>
>  > remote: Enumerating objects: 8, done.
>
>  > remote: Counting objects: 100% (8/8), done.
>
>  > remote: Compressing objects: 100% (4/4), done.
>
>  > remote: Total 46 (delta 4), reused 5 (delta 4), pack-reused 38
>
>  > Receiving objects: 100% (46/46), 178.32 KiB | 1.29 MiB/s, done.
>
>  > Resolving deltas: 100% (12/12), done.
>
>  user@PCname MINGW64 ***/e/(...)/Manuales/sesion4***
>
>  ***$ ls -la***
>
>  > total 4
>
>  > drwxr-xr-x 1 user 197609 0 nov.  5 15:34 ./
>
>  > drwxr-xr-x 1 user 197609 0 nov.  5 16:01 ../
>
>  > drwxr-xr-x 1 user 197609 0 nov.  5 15:34 slide-components-litelement/
>
>  user@PCname MINGW64 ***/e/(...)/Manuales/sesion4***
>
>  ***$ cd slide-components-litelement/***
>
>  user@PCname MINGW64 ***/e/(...)/Manuales/sesion4/slide-components-litelement (master)***
>
>  ***$ ls -la***
>
>  > total 164
>
>  > drwxr-xr-x 1 user 197609      0 nov.  5 15:34 ./
>
>  > drwxr-xr-x 1 user 197609      0 nov.  5 15:34 ../
>
>  > drwxr-xr-x 1 user 197609      0 nov.  5 15:39 .git/
>
>  > -rw-r--r-- 1 user 197609   1209 nov.  5 15:34 .gitignore
>
>  > -rw-r--r-- 1 user 197609    207 nov.  5 15:34 .npmignore
>
>  > drwxr-xr-x 1 user 197609      0 nov.  5 15:34 dist/
>
>  > -rw-r--r-- 1 user 197609    604 nov.  5 15:34 package.json
>
>  > -rw-r--r-- 1 user 197609 148365 nov.  5 15:34 package-lock.json
>
>  > -rw-r--r-- 1 user 197609    351 nov.  5 15:34 README.md
>
>  > -rw-r--r-- 1 user 197609    154 nov.  5 15:34 rollup.config.js
>
>  > drwxr-xr-x 1 user 197609      0 nov.  5 15:34 src/
>
----
>  user@PCname MINGW64 ***/e/(...)/Manuales/sesion4-2***
>
>  ***$ git clone https://github.com/EscuelaIt/slide-components-litelement.git .***
>
>  > Cloning into '.'...
>
>  > remote: Enumerating objects: 8, done.
>
>  > remote: Counting objects: 100% (8/8), done.
>
>  > remote: Compressing objects: 100% (4/4), done.
>
>  > remote: Total 46 (delta 4), reused 5 (delta 4), pack-reused 38
>
>  > Receiving objects: 100% (46/46), 178.32 KiB | 461.00 KiB/s, done.
>
>  > Resolving deltas: 100% (12/12), done.
>
>  user@PCname MINGW64 ***/e/(...)/Manuales/sesion4-2 (master)***
>
>  ***$ ls -la***
>
>  > total 164
>
>  > drwxr-xr-x 1 user 197609      0 nov.  5 16:02 ./
>
>  > drwxr-xr-x 1 user 197609      0 nov.  5 16:01 ../
>
>  > drwxr-xr-x 1 user 197609      0 nov.  5 16:02 .git/
>
>  > -rw-r--r-- 1 user 197609   1209 nov.  5 16:02 .gitignore
>
>  > -rw-r--r-- 1 user 197609    207 nov.  5 16:02 .npmignore
>
>  > drwxr-xr-x 1 user 197609      0 nov.  5 16:02 dist/
>
>  > -rw-r--r-- 1 user 197609    604 nov.  5 16:02 package.json
>
>  > -rw-r--r-- 1 user 197609 148365 nov.  5 16:02 package-lock.json
>
>  > -rw-r--r-- 1 user 197609    351 nov.  5 16:02 README.md
>
>  > -rw-r--r-- 1 user 197609    154 nov.  5 16:02 rollup.config.js
>
>  > drwxr-xr-x 1 user 197609      0 nov.  5 16:02 src/
----

+ Veremos además una operativa que es muy básica después de clonar el repositorio y antes de poder arrancar el proyecto, que es ***la instalación de sus dependencias***. Lo veremos con un proyecto Javascript que tiene ***dependencias de npm***.

----
> E:\(...)\Manuales\sesion4\slide-components-litelement>***npm install***
>
> > npm WARN slide-down-elements-app@ No repository field.
>
> > added 411 packages from 280 contributors and audited 411 packages in 123.725s
>
> > 2 packages are looking for funding
>
> >  run `npm fund` for details
>
> > found 2 vulnerabilities (1 moderate, 1 high)
> >  run `npm audit fix` to fix them, or `npm audit` for details
>
+  [Volver al INDICE](#markdown-header-indice)

### 5.- Subir y actualizar un repositorio Git en local a GitHub y BitBucket ###
+ Vamos a ver las operativas básicas que se realizan para trabajar con repositorios en local y subirlo a un repositorio en remoto. Es decir, cómo subir un repositorio a GitHub, Bitbucket o cualquier hosting de repositorio remoto, enviar código que hemos actualizado en local hacia remoto (push) y cómo recuperar actualizaciones realizadas en remoto para traerlas a local (pull).
+ Ahora vamos a realizar de manera práctica toda la operativa esencial de trabajo con repositorios locales y remotos (en GitHub):
    + Crear un repositorio en local y subirlo a Github (remote add)
    + Enviar cambios a Github realizados en local (push)
    + Traerse cambios del repositorio remoto a local (pull)
    + Cómo bajarse un clon de repositorio y enviar cambios

+ ***SUBIR NUESTRO PROYECTO EN GIT A BITBUCKET***
     + Para subir nuestro proyecto en git con BitBucket, podemos seguir los siguientes pasos sin complicarnos la vida de la forma más simple.
        + Nota: Para eliminar los archivos .git de un proyecto (local)
>  $rm -rf .git
>
+ ***5.1.-Creamos el repositorio GIT (local)***
>
> $git init
//creamos .gitignore
>
> $nano .gitignore
> 
+ Agregamos:
*.pdf
*.sql
>
> $git status //ver el estado de los archivos deberia de mostrar que 
>
+ los archivos aun faltan agregar
>
> $git add -A //Agregar todo los archivos
>
> $git commit -m “Inicializacion de versionamiento del nuevo repositorio”
>
> $git status
>
> > On branch master
>
> > nothing to commit (working directory clean)
>
+ ***5.2.- Enlazamos el git remoto de bitbucket (Remoto)***
>
> $git remote add origin https://<MI_USUARIO>@bitbucket.org/<MI_PROYECTO>/<REPOSITORIO>.git
>
> $git remote -v //Podremos ver las url a donde están apuntando
>
> $cat .git/config //Otra forma de ver la configuración
>
+ Nota: si deseamos eliminar el origen (origin remote url), ejecutamos:
>    
    > $git remote rm origin
>    
+ (y continuamos desde el inicio del paso 2 si es que hemos optado por eliminar y querer volver a especificar el origen) 
+ Subimos los archivos al git remote
>
> $git push -u origin master
>
+ ***5.3.-Subiendo un cambio de local a remoto.***
     + Primero guardamos los cambios en local
>
> $git status
>
> $git add -A
>
> $git commit -m “se guarda los cambios”
>
> $git status
>
+ ***5.4.- Subimos los cambios al bitbucket***
>
> $git push origin master
>
+ ***5.5.-Bajando los cambios (otro usuario)***
>
> $git pull origin master
>
+ ***5.6.-Bajando los cambios (todos los branch’s)***
>
> $git pull
>
+ ***5.7.-Eliminar branch en el repositorio remoto***
>
> $git push –delete  origin <branch_name>
>
***Ejemplo:***
> E:\(...)\Manuales\sesion4-2 (master -> origin) (slide-down-elements-app)
> ? ***rm -rf .git***
> E:\(...)\Manuales\sesion4-2 (slide-down-elements-app)
> ? ***git init***
> Initialized empty Git repository in E:/altia-proy/202011_Curso_DOTNET/Manuales/sesion4-2/.git/
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
> ? ***nano .gitignore***
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
> ? ***git status***
> > On branch master
> > No commits yet
> > Untracked files:
> >  (use "git add <file>..." to include in what will be committed)
> >     .gitignore
> >     .npmignore
> >     README.md
> >     dist/
> >     package-lock.json
> >     package.json
> >     rollup.config.js
> >     src/
> > nothing added to commit but untracked files present (use "git add" to track)
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
> ? ***git add -A***
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
> ? ***git commit -m "Inicializacion de versionamiento del nuevo repositorio"***
> > [master (root-commit) d3a4ae6] Inicializacion de versionamiento del nuevo repositorio
> >  21 files changed, 4826 insertions(+)
> >  create mode 100644 .gitignore
> >  create mode 100644 .npmignore
> >  create mode 100644 README.md
> >  create mode 100644 dist/index.html
> >  create mode 100644 dist/index.js
> >  create mode 100644 dist/index.js.map
> >  create mode 100644 dist/legacy/index.js
> >  create mode 100644 dist/legacy/index.js.map
> >  create mode 100644 dist/polyfills/babel-polyfills.js
> >  create mode 100644 dist/polyfills/dynamic-import-polyfill.js
> >  create mode 100644 dist/polyfills/system-loader.js
> >  create mode 100644 dist/polyfills/webcomponent-polyfills.js
> >  create mode 100644 package-lock.json
> >  create mode 100644 package.json
> >  create mode 100644 rollup.config.js
> >  create mode 100644 src/index.html
> >  create mode 100644 src/js/components/eit-ver-mas.js
> >  create mode 100644 src/js/components/menu-overlay.js
> >  create mode 100644 src/js/components/menu-responsive.js
> >  create mode 100644 src/js/index.js
> >  create mode 100644 src/js/mixins/slide-down-mixin.js
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
> ? ***git statusv***
> > On branch master
> > nothing to commit, working tree clean
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
> ? ***git remote add origin https://dolofc@bitbucket.org/dolofc/sesion5-slide-down-elements-app.git***
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
> ? ***git remote -v***
> > origin https://dolofc@bitbucket.org/dolofc/sesion5-slide-down-elements-app.git (fetch)
> > origin https://dolofc@bitbucket.org/dolofc/sesion5-slide-down-elements-app.git (push)
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
> ? ***cat .git/config***
> > [core]
> >     repositoryformatversion = 0
> >     filemode = false
> >     bare = false
> >     logallrefupdates = true
> >     symlinks = false
> >     ignorecase = true
> > [remote "origin"]
> >     url = https://dolofc@bitbucket.org/dolofc/sesion5-slide-down-elements-app.git
> >     fetch = +refs/heads/*:refs/remotes/origin/*
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
> ? ***ls -la***
> > total 164
> > drwxr-xr-x 1 dolores 197609   0 nov. 9 13:17 ./
> > drwxr-xr-x 1 dolores 197609   0 nov. 5 16:01 ../
> > drwxr-xr-x 1 dolores 197609   0 nov. 9 13:40 .git/
> > -rw-r--r-- 1 dolores 197609  1209 nov. 5 15:34 .gitignore
> > -rw-r--r-- 1 dolores 197609  207 nov. 5 16:02 .npmignore
> > drwxr-xr-x 1 dolores 197609   0 nov. 5 16:02 dist/
> > -rw-r--r-- 1 dolores 197609  604 nov. 5 16:02 package.json
> > -rw-r--r-- 1 dolores 197609 148365 nov. 5 16:02 package-lock.json
> > -rw-r--r-- 1 dolores 197609  351 nov. 5 16:02 README.md
> > -rw-r--r-- 1 dolores 197609  154 nov. 5 16:02 rollup.config.js
> > drwxr-xr-x 1 dolores 197609   0 nov. 5 16:02 src/
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
> ? ***git push -u origin master***
> > Enumerating objects: 30, done.
> > Counting objects: 100% (30/30), done.
> > Delta compression using up to 4 threads
> > Compressing objects: 100% (29/29), done.
> > Writing objects: 100% (30/30), 172.94 KiB | 2.93 MiB/s, done.
> > Total 30 (delta 2), reused 0 (delta 0), pack-reused 0
> > To https://bitbucket.org/dolofc/sesion5-slide-down-elements-app.git
> >  * [new branch]   master -> master
> > Branch 'master' set up to track remote branch 'master' from 'origin'.
> E:\(...)\Manuales\sesion4-2 (***master -> origin***) (slide-down-elements-app)
> ? 
>
+ ***EJEMPLO***
> E:\(...)\Manuales\sesion4-2 (master -> origin) (slide-down-elements-app)
>
> ? ***rm -rf .git***
>
> E:\(...)\Manuales\sesion4-2 (slide-down-elements-app)
>
> ? ***git init***
>
> Initialized empty Git repository in E:/altia-proy/202011_Curso_DOTNET/Manuales/sesion4-2/.git/
>
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
>
> ? ***nano .gitignore***
>
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
>
> ? ***git status***
>
> > On branch master
>
> > No commits yet
>
> > Untracked files:
>
> >  (use "git add <file>..." to include in what will be committed)
>
> >     .gitignore
>
> >     .npmignore
>
> >     README.md
>
> >     dist/
>
> >     package-lock.json
>
> >     package.json
>
> >     rollup.config.js
>
> >     src/
>
> > nothing added to commit but untracked files present (use "git add" to track)
>
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
>
> ? ***git add -A***
>
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
>
> ? ***git commit -m "Inicializacion de versionamiento del nuevo repositorio"***
>
> > [master (root-commit) d3a4ae6] Inicializacion de versionamiento del nuevo repositorio
>
> >  21 files changed, 4826 insertions(+)
>
> >  create mode 100644 .gitignore
>
> >  create mode 100644 .npmignore
>
> >  create mode 100644 README.md
>
> >  create mode 100644 dist/index.html
>
> >  create mode 100644 dist/index.js
>
> >  create mode 100644 dist/index.js.map
>
> >  create mode 100644 dist/legacy/index.js
>
> >  create mode 100644 dist/legacy/index.js.map
>
> >  create mode 100644 dist/polyfills/babel-polyfills.js
>
> >  create mode 100644 dist/polyfills/dynamic-import-polyfill.js
>
> >  create mode 100644 dist/polyfills/system-loader.js
>
> >  create mode 100644 dist/polyfills/webcomponent-polyfills.js
>
> >  create mode 100644 package-lock.json
>
> >  create mode 100644 package.json
>
> >  create mode 100644 rollup.config.js
>
> >  create mode 100644 src/index.html
>
> >  create mode 100644 src/js/components/eit-ver-mas.js
>
> >  create mode 100644 src/js/components/menu-overlay.js
>
> >  create mode 100644 src/js/components/menu-responsive.js
>
> >  create mode 100644 src/js/index.js
>
> >  create mode 100644 src/js/mixins/slide-down-mixin.js
>
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
>
> ? ***git statusv***
>
> > On branch master
>
> > nothing to commit, working tree clean
>
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
>
> ? ***git remote add origin https://dolofc@bitbucket.org/dolofc/sesion5-slide-down-elements-app.git***
>
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
>
> ? ***git remote -v***
>
> > origin https://dolofc@bitbucket.org/dolofc/sesion5-slide-down-elements-app.git (fetch)
>
> > origin https://dolofc@bitbucket.org/dolofc/sesion5-slide-down-elements-app.git (push)
>
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
>
> ? ***cat .git/config***
>
> > [core]
>
> >     repositoryformatversion = 0
>
> >     filemode = false
>
> >     bare = false
>
> >     logallrefupdates = true
>
> >     symlinks = false
>
> >     ignorecase = true
>
> > [remote "origin"]
>
> >     url = https://dolofc@bitbucket.org/dolofc/sesion5-slide-down-elements-app.git
>
> >     fetch = +refs/heads/*:refs/remotes/origin/*
>
> E:\(...)\Manuales\sesion4-2 (master) (slide-down-elements-app)
>
> ? ***ls -la***
>
> > total 164
>
> > drwxr-xr-x 1 dolores 197609   0 nov. 9 13:17 ./
>
> > drwxr-xr-x 1 dolores 197609   0 nov. 5 16:01 ../
>
> > drwxr-xr-x 1 dolores 197609   0 nov. 9 13:40 .git/
>
> > -rw-r--r-- 1 dolores 197609  1209 nov. 5 15:34 .gitignore
>
> > -rw-r--r-- 1 dolores 197609  207 nov. 5 16:02 .npmignore
>
> > drwxr-xr-x 1 dolores 197609   0 nov. 5 16:02 dist/
>
> > -rw-r--r-- 1 dolores 197609  604 nov. 5 16:02 package.json
>
> > -rw-r--r-- 1 dolores 197609 148365 nov. 5 16:02 package-lock.json
>
> > -rw-r--r-- 1 dolores 197609  351 nov. 5 16:02 README.md
>
> > -rw-r--r-- 1 dolores 197609  154 nov. 5 16:02 rollup.config.js
>
> > drwxr-xr-x 1 dolores 197609   0 nov. 5 16:02 src/
>
> E:\(...)\Manuales\sesion4-2 (***master***) (slide-down-elements-app)
> λ ***git branch -M main***
>
> E:\(...)\Manuales\sesion4-2 (***main***) (slide-down-elements-app)>
>
> ? ***git push -u origin main***
>
> > Enumerating objects: 30, done.
>
> > Counting objects: 100% (30/30), done.
>
> > Delta compression using up to 4 threads
>
> > Compressing objects: 100% (29/29), done.
>
> > Writing objects: 100% (30/30), 172.94 KiB | 2.93 MiB/s, done.
>
> > Total 30 (delta 2), reused 0 (delta 0), pack-reused 0
>
> > To https://bitbucket.org/dolofc/sesion5-slide-down-elements-app.git
>
> >  * [new branch]   main -> main
>
> > Branch 'main' set up to track remote branch 'main' from 'origin'.
>
> > E:\(...)\Manuales\sesion4-2 (***main -> origin***) (slide-down-elements-app)>

+  [Volver al INDICE](#markdown-header-indice)

### 6.- .gitignore y borrado de archivos del repositorio ###
+ Aprende a ignorar archivos del working directory para que no se introduzcan en el repositorio de Git. Cómo borrar archivos que has hecho commit y no deberían estar en Git.
     + ***.gitignore***  [gitignore.io](https://www.toptal.com/developers/gitignore) Create useful .gitignore files for your project
+ Operativa muy sencilla pero muy útil sobre la gestión de repositorios de Git, como es la de ignorar determinados archivos o carpetas, de modo que no se envíen al repositorio al hacer los commits. 

> ?E:\(...)\Manuales\sesion4\slide-components-litelement [master =]
> 
> Î»  ***git log***
> 
> > commit 560a99b6816ceba14b4f155faa9d463429576994 (HEAD -> master, origin/master, origin/HEAD)
> 
> > Author: midesweb <malvarez@desarrolloweb.com>
> 
> > Date:   Tue Oct 6 15:57:14 2020 +0200
> 
> >     npm audit fix
> 
> > commit d0a604eb5fcd4db6c819c96d56d732ca7291c250
> 
> > Author: Miguel Angel Alvarez <malvarez@desarrolloweb.com>
> 
> > Date:   Thu Jun 13 21:23:05 2019 +0200
> 
> >     Enlace al demo
> 
> > commit 6ab0fb67f5865a61b5db067966467eaf55e4d31d
> 
> > Author: Miguel Angel Alvarez <malvarez@desarrolloweb.com>
> 
> > Date:   Thu Jun 13 21:21:35 2019 +0200
> 
> >     explicando componentes
> 
> > commit ad53c0a533f9782e501584e334f5893230124cc2
> 
> > Author: Miguel Angel Alvarez <malvarez@desarrolloweb.com>
> 
> > Date:   Thu Jun 13 21:14:34 2019 +0200
> 
> >     1st commit
> 
> ?E:\(...)\Manuales\sesion4\slide-components-litelement [master =]
> 
> Î»  ***git log --oneline***
> 
> > 560a99b (HEAD -> master, origin/master, origin/HEAD) npm audit fix
> 
> > d0a604e Enlace al demo
> 
> ?E:\(...)\Manuales\sesion4\slide-components-litelement [master =]
> 
> Î»  ***git ls-tree --full-tree HEAD***
> 
> > 100644 blob f62685251bd58e2bf516c7835234d8a1120b7297    .gitignore
> 
> > 100644 blob 79aba724fabcc2e918934cc2359c9765e97aab63    .npmignore
> 
> > 100644 blob 3983f21fa2178165353fa5234a11d1bd67073db1    README.md
> 
> > 040000 tree e360f5742e8fdc9cb862fa53bc2ef52b340a7ae5    dist
> 
> > 100644 blob aaaaea28360066fd192361d4552ebc0006f36163    package-lock.json
> 
> > 100644 blob 9253721ad4dc3ccd2df46adff6674f36c7adcb87    package.json
> 
> > 100644 blob c1f2fe42d6070cdc0e7241bbda7a8e4d632d9f45    rollup.config.js
> 
> > 040000 tree c443615d67372fe2e805701576fe9d5a00806626    src
> 
> > 6ab0fb6 explicando componentes
> 
> > ad53c0a 1st commit
> 
> ?E:\(...)\Manuales\sesion4\slide-components-litelement [master =]
> 

+ Solucionar los casos en los que hayas enviado un archivo al repositorio por error y lo necesites borrar.
     + [Eliminar archivos de un repositorio git después de ignorarlos con .gitignore](https://desarrolloweb.com/articulos/eliminar-archivos-git-gitignore.html)
     + Si quieres que el archivo permanezca en tu ordenador pero simplemente que se borre del repostorio tienes que hacerlo así.
     > ***git rm --cached nombre_archivo***
     + Si lo que se desea es borrar un directorio y todos sus contenidos, hay que hacer algo así:
     > ***git rm -r --cached nombre_directorio***
     + El parámetro --cached lo elimina del repositorio, pero lo deja dentro del ***Working Directory***
+  [Volver al INDICE](#markdown-header-indice)

### 8.- Ramas y GitHub ###
 + Operaciones con ramas entre local y remoto, entre el repositorio que tenemos instalado en nuestro equipo y en este caso GitHub o Bitbucket.
     + git branch ***Crear ramas***
     + git show-branch ***Muestra todas las ramas del proyecto***
     + git checkout ***Pasar de una rama a otra***
     + git checkout -b otrarama  ***crear una rama nueva y moverse a ella en un único paso***
 + Dos operativas básicas de trabajo con ramas y repositorios remotos, como GitHub o cualquier otro proveedor de posting para Git.
     + Cómo enviar ramas a Github con línea de comandos
     + Cómo enviar ramas a GitHub con el editor
     + Cómo enviar ramas y definir la rama predeterminada de seguimiento
     + Cómo traernos ramas desde GitHub a local.

+  [Volver al INDICE](#markdown-header-indice)

### 7.- Fusionar ramas con Git merge ###
+ Cómo fusionar las ramas de un proyecto, de manera que unifiquemos todos los cambios de una rama sobre otra rama del proyecto llevado con Git. Cómo resolver conflictos en el merge.
+ Fusionar ramas. Crearemos ramas dentro de un proyecto, realizaremos cambios en ellas y luego las fusionaremos con “git merge". Eventualmente algunos cambios producirán conflictos que tendremos que resolver para hacer la fusión de las ramas correctamente.
+ [Volver al INDICE](#markdown-header-indice)

### 9.- Pull request ###
+ Qué es la operación de Pull Request en un repositorio. Cómo organizar el trabajo con ramas y el fusionado con la rama máster (o main). Cómo colaborar en repositorios de otras personas mediante el fork y pull request.
+  [Volver al INDICE](#markdown-header-indice)

### 10.- Publicar prácticas de cursos a través del sistema de GitHub Classroom  ###
+ En este vídeo vamos a ver todo el proceso de publicación de una práctica en GitHub Classroom, para que podáis ver cómo funciona y no tener dudas a la hora de presentar trabajos.
+ Pasos para enviarnos una práctica a través de Github Classroom:
    + Recibir un enlace para aceptar la tarea (assignment)
    + Asociar vuestro usuario de GitHub con el usuario de la plataforma de EscuelaIT (esto se tiene que hacer una vez y ya no es necesario realizar este paso en los siguientes assignments)
    + Ver el enunciado de la tarea en el README.md
    + Clonar el repositorio donde vais a presentar la práctica
    + Realizar la práctica
    + Subir los cambios a HutHub haciendo el commit/s y luego el push

+  [Volver al INDICE](#markdown-header-indice)